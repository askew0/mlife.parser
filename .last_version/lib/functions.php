<?
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Functions {
	
	//func array(<ид свойства>,<ид элемента>,<ид инфоблока>, <строка для обработки>, <параметры функции строка>)
	
	public function setNumeric ($propId,$elId,$ibId,$data,$userparam){
		
		$regeg = explode("|||",$userparam);
		if(count($regeg)==2) {
			$data = preg_replace("/".$regeg[0]."/is",$regeg[1],$data);
		}
		$data = intval($data);
		if($data){
			return $data;
		}else{
			return false;
		}
		
	}
	
	public function setString ($propId,$elId,$ibId,$data,$userparam){
		
		$regeg = explode("|||",$userparam);
		if(count($regeg)==2) {
			$data = preg_replace("/".$regeg[0]."/is",$regeg[1],$data);
		}
		$data = trim($data);
		if($data){
			return $data;
		}else{
			return false;
		}
		
	}
	
	public function setCheckBox ($propId,$elId,$ibId,$data,$userparam) {
		
		$params = explode("|||",$userparam);
		$ibCur = $ibId;
		
		$propid = $propId;
		
		$str = strlen($data);
		$val = '';
		if($str>0) {
			if(count($params)==2){
				if(trim($data)==$params[1]){
					$val = $params[0];
				}else{
					$val = "";
				}
			}elseif(count($params)==1){
				$val = $params[0];
			}else{
				$val = "Y";
			}
		}
		if($val) {
			
			$db_enum_list = \CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>$ibCur, "PROPERTY_ID" => $propid, "VALUE"=>$val));
			if($ar_enum_list = $db_enum_list->GetNext())
			{
				return $ar_enum_list["ID"];
			}
			
		}
		return false;
		
	}
	
	public function SetOption ($propId,$elId,$ibId,$data,$userparam) {
	
		$regeg = explode("|||",$userparam);
		if(count($regeg)==2) {
			$data = preg_replace("/".$regeg[0]."/is",$regeg[1],$data);
		}
		
		$propid = $propId;
		
		$ibCur = $ibId;
		
		if($data) {
			$db_enum_list = \CIBlockPropertyEnum::GetList(array(), Array("IBLOCK_ID"=>$ibCur, "PROPERTY_ID" => $propid, "VALUE"=>trim($data)));
			while($ar_enum_list = $db_enum_list->GetNext())
			{
				if($ar_enum_list["VALUE"]){
					return $ar_enum_list["ID"];
				}
			}
			
			$ibpenum = new \CIBlockPropertyEnum;
			if($PropID = $ibpenum->Add(Array('PROPERTY_ID'=>$propid, 'VALUE'=>trim($data)))) {
				return $PropID;
			}
			
		}
		return false;
		
	}
	
	public function SetOptions ($propId,$elId,$ibId,$data,$userparam) {
		
		if(!$userparam) $userparam = ",|||xxx";
		$regeg = explode("|||",$userparam);
		if(count($regeg)==2) {
			//$data = preg_replace("/".$regeg[0]."/is",$regeg[1],$data);
			$dataTemp = explode($regeg[0],$data);
			$dataAr = array();
			foreach($dataTemp as $val) {
				if(strpos($val,$regeg[1])!==false){
					
				}else{
					$dataAr[] = trim($val);
				}
			}
		}
		
		$propid = $param["PROP_ID"];
		
		$ibCur = $ibId;
		
		$finData = array();
		
		if(count($dataAr)>0) {
			
			foreach($dataAr as $data){
				$nnn = false;
				$db_enum_list = \CIBlockPropertyEnum::GetList(array(), Array("IBLOCK_ID"=>$ibCur, "PROPERTY_ID" => $propid, "VALUE"=>trim($data)));
				while($ar_enum_list = $db_enum_list->GetNext())
				{
					if($ar_enum_list["VALUE"]){
						$finData[] = array("VALUE"=>$ar_enum_list["ID"]);
						$nnn = true;
					}
					
				}
				if(!$nnn) {
					$ibpenum = new \CIBlockPropertyEnum;
					if($PropID = $ibpenum->Add(Array('PROPERTY_ID'=>$propid, 'VALUE'=>$data))) {
						$finData[] = array("VALUE"=>$PropID);
					}
				}
			}
			
		}
		
		if(count($finData)>0) return $finData;
		
		return false;
		
	}
	
	public function setHtmlProp($propId,$elId,$ibId,$data,$userparam){
		
		return array('VALUE'=>array('TYPE'=>'HTML','TEXT'=>$data));
		
	}
	
	public function setDetailImage ($propId,$elId,$ibId,$data,$userparam){
		
		return \CFile::MakeFileArray($data);
		
	}
	
}
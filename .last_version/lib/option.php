<?
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Option {
	
	//����� � ��������� ��������� �� ���������
	public static $CLASS_FUNCTIONS = '\Mlife\Parser\Functions';
	
	//html ����� � ����������� ��� �������
	public function getParamsHtml($arParams = array(), $iblockId=false){
		
		$context = \Bitrix\Main\Application::getInstance()->getContext();
		$request = $context->getRequest();
		
		if(!$iblockId){
			$iblockId = 0;
			if($request->get('iblock',0)>0){
				$iblockId = $request->get('iblock',0);
			}elseif($request->get('iblockid',0)>0) {
				$iblockId = $request->get('iblockid',0);
			}
		}
		
		$res = \Bitrix\Iblock\PropertyTable::getList(
			array(
				"select" => array("NAME","ID","PROPERTY_TYPE","CODE"),
				"filter" => array("IBLOCK_ID"=>$iblockId)
			)
		);
		$arProps = array();
		$arPropsAdded = array();
		while($propAr = $res->Fetch()){
			$arProps[$propAr["ID"]] = $propAr["NAME"]." [".$propAr["CODE"]."] [".$propAr["PROPERTY_TYPE"]."]";
			if($propAr["CODE"]=="YANDEX_ID" || $propAr["CODE"]=="YA_ACTIVE_HAR" || $propAr["CODE"]=="YA_ACTIVE_IMG") $arPropsAdded[] = $propAr["CODE"];
		}
		if(count($arPropsAdded)!=3 && $iblockId){
			\CModule::IncludeModule("iblock");
			if(!in_array("YANDEX_ID",$arPropsAdded)){
				$arFields = Array(
				  "NAME" => Loc::getMessage("MLIFE_PARSER_OPTION_YANDEX_ID"),
				  "ACTIVE" => "Y",
				  "SORT" => "600",
				  "CODE" => "YANDEX_ID",
				  "PROPERTY_TYPE" => "S",
				  "IBLOCK_ID" => $iblockId
				);
				  
				$ibp = new \CIBlockProperty;
				$PropID = $ibp->Add($arFields);
			}
			if(!in_array("YA_ACTIVE_HAR",$arPropsAdded)){
				$arFields = Array(
				  "NAME" => Loc::getMessage("MLIFE_PARSER_OPTION_YA_ACTIVE_HAR"),
				  "ACTIVE" => "Y",
				  "SORT" => "600",
				  "CODE" => "YA_ACTIVE_HAR",
				  "PROPERTY_TYPE" => "L",
				  "IBLOCK_ID" => $iblockId
				);
				$arFields["VALUES"][0] = Array(
				  "VALUE" => "Y",
				  "DEF" => "N",
				  "SORT" => "100"
				);
				  
				$ibp = new \CIBlockProperty;
				$PropID = $ibp->Add($arFields);
			}
			if(!in_array("YA_ACTIVE_IMG",$arPropsAdded)){
				$arFields = Array(
				  "NAME" => Loc::getMessage("MLIFE_PARSER_OPTION_YA_ACTIVE_IMG"),
				  "ACTIVE" => "Y",
				  "SORT" => "600",
				  "CODE" => "YA_ACTIVE_IMG",
				  "PROPERTY_TYPE" => "L",
				  "IBLOCK_ID" => $iblockId
				);
				$arFields["VALUES"][0] = Array(
				  "VALUE" => "Y",
				  "DEF" => "N",
				  "SORT" => "100"
				);
				
				$ibp = new \CIBlockProperty;
				$PropID = $ibp->Add($arFields);
			}
		}
		$arProps["EL_PREVIEW_PICTURE"] = Loc::getMessage("MLIFE_PARSER_OPTION_T2");
		$arProps["EL_DETAIL_PICTURE"] = Loc::getMessage("MLIFE_PARSER_OPTION_T1");
		foreach($arProps as $key=>$prop){
			if(!is_array($arParams["PARAM_".$key])) $arParams["PARAM_".$key] = array("","","");
		}
		
		$selectFunc = self::getFunctionsList();
		
		$html = '';
		$html = '<tr><td colspan="2">';
		$html .= '<table>';
		$html .= '<tr class="heading">
		<td>'.Loc::getMessage("MLIFE_PARSER_OPTION_T3").'</td>
		<td>'.Loc::getMessage("MLIFE_PARSER_OPTION_T4").'</td>
		<td>'.Loc::getMessage("MLIFE_PARSER_OPTION_T5").'</td>
		<td>'.Loc::getMessage("MLIFE_PARSER_OPTION_T6").'</td>
		</tr>';
		foreach($arProps as $propId=>$prop){
			$html .= '<tr>';
			$html .= '<td>'.$prop.'</td>
			<td><input type="text" name="PARAM_'.$propId.'[]" value="'.$arParams['PARAM_'.$propId][0].'"/></td>
			<td><select name="PARAM_'.$propId.'[]">'.self::getActiveSelectFunctions($selectFunc,$arParams['PARAM_'.$propId][1]).'</select></td>
			<td><input type="text" name="PARAM_'.$propId.'[]" value="'.$arParams['PARAM_'.$propId][2].'"/></td>';
			$html .= '</tr>';
		}
		$html .= '</td></tr>';
		$html .= '</table>';
		
		return $html;
		
	}
	
	//������ ������� ��� ��������� ������
	public function getFunctionsList(){
		
		$className = self::$CLASS_FUNCTIONS;
		$func = get_class_methods($className);
		$funcAr = array();
		foreach($func as $f_name) {
			$funcAr[] = $f_name;
		}
		return $funcAr;
		
	}
	
	//��������� html ����� ������� � ������ ����������
	public function getActiveSelectFunctions($selectFunc,$active=false){
		$sel = (!$active) ? " selected=\"selected\"" : "";
		$selectHtml = '<option value=""'.$sel.'>'.Loc::getMessage("MLIFE_PARSER_OPTION_T7").'</option>';
		foreach($selectFunc as $name){
			$sel = ($name==$active) ? " selected=\"selected\"" : "";
			$selectHtml .= '<option value="'.$name.'"'.$sel.'>'.$name.'</option>';
		}
		return $selectHtml;
	}
	
	public function getNote(){
		
		return "";
		
	}
	
}

?>
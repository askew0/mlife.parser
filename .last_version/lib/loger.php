<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class LogerTable extends Entity\DataManager
{
	public static function getFilePath()
	{
		return __FILE__;
	}
	
	public static function getTableName()
	{
		return 'mlife_parser_loger';
	}
	
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('MLIFE_PARSER_LOGER_ENTITY_ID_FIELD'),
			),
			'PARSER' => array(
				'data_type' => 'string',
				'required' => true,
				'title' => Loc::getMessage('MLIFE_PARSER_LOGER_ENTITY_PARSER_FIELD'),
			),
			'PROFILE' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('MLIFE_PARSER_LOGER_ENTITY_PROFILE_FIELD'),
			),
			'TEXT' => array(
				'data_type' => 'string',
				'required' => true,
				'title' => Loc::getMessage('MLIFE_PARSER_LOGER_ENTITY_TEXT_FIELD'),
			),
		);
	}
	
	public function clearLog($parser,$profile){
		
		$entity = \Mlife\Parser\LogerTable::getEntity();
		$result = new Entity\Result();
		
		// delete
		$connection = \Bitrix\Main\Application::getConnection();
		$helper = $connection->getSqlHelper();

		$tableName = $entity->getDBTableName();

		$where = 'PARSER="'.$parser.'" AND PROFILE='.$profile;

		$sql = "DELETE FROM ".$tableName." WHERE ".$where;
		
		$connection->queryExecute($sql);

		return $result;
		
	}

}
<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser\Options\universal;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Option extends \Mlife\Parser\Option
{
	
	const CLASS_PROFILE = '\Mlife\Parser\Profile\UniversalTable'; //класс с описанием сущности со списком профилей
	const CLASS_PARSER = '\Mlife\Parser\Options\universal\Parser'; //класс парсера
	
	function __construct() {
		parent::$CLASS_FUNCTIONS = 'Mlife\Parser\Options\universal\Functions'; //пользовательские функции для обработки данных
	}
	
	public static function getMenu() {
		
		$menu = array(
			array(
				"text" => Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_MENU1"),
				"url" => "mlife_parser_profile.php?lang=".LANGUAGE_ID."&parser=universal",
				"more_url" => Array("mlife_parser_profile_edit.php?lang=".LANGUAGE_ID."&parser=universal"),
				"title" => Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_MENU1"),
				"sort" => 100,
			),
		);
		
		return $menu;
		
	}
	
	public function getCtMenu($id,$lang,$parser) {
		
		$arActions = array();
		$arActions[] = array(
			"ICON"=>"edit",
			"DEFAULT"=>true,
			"TEXT"=>Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_MENU6"),
			"TITLE"=>Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_MENU6"),
			"ACTION"=>'mlife_parser_profile_edit.php?ID='.$id.'&lang='.$lang."&parser=".$parser
			);
		$arActions[] = array(
			"ICON"=>"edit",
			"DEFAULT"=>true,
			"TEXT"=> Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_MENU7"),
			"TITLE"=> Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_MENU7"),
			"ACTION"=>'mlife_parser_start.php?ID='.$id.'&lang='.$lang."&parser=".$parser."&DELETELOG=1&option=get_count"
			);
		$arActions[] = array(
			"ICON"=>"edit",
			"DEFAULT"=>true,
			"TEXT"=> Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_MENU5"),
			"TITLE"=> Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_MENU5"),
			"ACTION"=>'mlife_parser_start.php?ID='.$id.'&lang='.$lang."&parser=".$parser."&DELETELOG=1&option=set_url"
			);
		
		return $arActions;
		
	}
	
	public function getNote(){
		
		$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_UNIVERSAL_MESS");
		
		return $mess;
		
	}
	
	public function getParamsHtml($arParams = array(), $iblockId=false){
		
		$context = \Bitrix\Main\Application::getInstance()->getContext();
		$request = $context->getRequest();
		
		if(!$iblockId){
			$iblockId = 0;
			if($request->get('iblock',0)>0){
				$iblockId = $request->get('iblock',0);
			}elseif($request->get('iblockid',0)>0) {
				$iblockId = $request->get('iblockid',0);
			}
		}
		
		$res = \Bitrix\Iblock\PropertyTable::getList(
			array(
				"select" => array("NAME","ID","PROPERTY_TYPE","CODE"),
				"filter" => array("IBLOCK_ID"=>$iblockId)
			)
		);
		$arProps = array();
		$arPropsAdded = array();
		while($propAr = $res->Fetch()){
			$arProps[$propAr["ID"]] = $propAr["NAME"]." [".$propAr["CODE"]."] [".$propAr["PROPERTY_TYPE"]."]";
			if($propAr["CODE"]=="PARSER_LINK" || $propAr["CODE"]=="UNIVERSAL_UP") $arPropsAdded[] = $propAr["CODE"];
		}
		if(count($arPropsAdded)!=2 && $iblockId){
			\CModule::IncludeModule("iblock");
			if(!in_array("PARSER_LINK",$arPropsAdded)){
				$arFields = Array(
				  "NAME" => Loc::getMessage("MLIFE_PARSER_OPTION_PARSER_LINK"),
				  "ACTIVE" => "Y",
				  "SORT" => "600",
				  "CODE" => "PARSER_LINK",
				  "PROPERTY_TYPE" => "S",
				  "IBLOCK_ID" => $iblockId
				);
				  
				$ibp = new \CIBlockProperty;
				$PropID = $ibp->Add($arFields);
			}
			if(!in_array("UNIVERSAL_UP",$arPropsAdded)){
				$arFields = Array(
				  "NAME" => Loc::getMessage("MLIFE_PARSER_OPTION_UNIVERSAL_UP"),
				  "ACTIVE" => "Y",
				  "SORT" => "600",
				  "CODE" => "UNIVERSAL_UP",
				  "PROPERTY_TYPE" => "L",
				  "IBLOCK_ID" => $iblockId
				);
				$arFields["VALUES"][0] = Array(
				  "VALUE" => "Y",
				  "DEF" => "N",
				  "SORT" => "100"
				);
				  
				$ibp = new \CIBlockProperty;
				$PropID = $ibp->Add($arFields);
			}
		}
		$arProps["EL_PREVIEW_PICTURE"] = Loc::getMessage("MLIFE_PARSER_OPTION_T2");
		$arProps["EL_DETAIL_PICTURE"] = Loc::getMessage("MLIFE_PARSER_OPTION_T1");
		$arProps["EL_DETAIL_TEXT"] = Loc::getMessage("MLIFE_PARSER_OPTION_T9");
		$arProps["EL_PREVIEW_TEXT"] = Loc::getMessage("MLIFE_PARSER_OPTION_T8");
		foreach($arProps as $key=>$prop){
			if(!is_array($arParams["PARAM_".$key])) $arParams["PARAM_".$key] = array("","","");
		}
		
		$selectFunc = self::getFunctionsList();
		
		$html = '';
		$html = '<tr><td colspan="2">';
		$html .= '<table>';
		$html .= '<tr class="heading">
		<td>'.Loc::getMessage("MLIFE_PARSER_OPTION_T3").'</td>
		<td>'.Loc::getMessage("MLIFE_PARSER_OPTION_T4").'</td>
		<td>'.Loc::getMessage("MLIFE_PARSER_OPTION_T5").'</td>
		<td>'.Loc::getMessage("MLIFE_PARSER_OPTION_T6").'</td>
		</tr>';
		foreach($arProps as $propId=>$prop){
			$html .= '<tr>';
			$html .= '<td>'.$prop.'</td>
			<td><input type="text" name="PARAM_'.$propId.'[]" value="'.$arParams['PARAM_'.$propId][0].'"/></td>
			<td><select name="PARAM_'.$propId.'[]">'.self::getActiveSelectFunctions($selectFunc,$arParams['PARAM_'.$propId][1]).'</select></td>
			<td><input type="text" name="PARAM_'.$propId.'[]" value="'.$arParams['PARAM_'.$propId][2].'"/></td>';
			$html .= '</tr>';
		}
		$html .= '</td></tr>';
		$html .= '</table>';
		$html .= '<tr class="heading"><td colspan="2">'.Loc::getMessage("MLIFE_PARSER_OPTION_UN_1");
		$html .= '</td></tr>';
		$html .= '<tr>
		<td>
			'.Loc::getMessage("MLIFE_PARSER_OPTION_UN_2").'
		</td>
		<td>
			<input type="text" name="PARAM_PARSE_SEL1" value="'.$arParams["PARAM_PARSE_SEL1"].'">
		</td>
		</tr>';
		$html .= '<tr>
		<td>
			'.Loc::getMessage("MLIFE_PARSER_OPTION_UN_3").'
		</td>
		<td>
			<input type="text" name="PARAM_PARSE_SEL2" value="'.$arParams["PARAM_PARSE_SEL2"].'">
		</td>
		</tr>';
		$html .= '<tr>
		<td>
			'.Loc::getMessage("MLIFE_PARSER_OPTION_UN_4").'
		</td>
		<td>
			<input type="text" name="PARAM_PARSE_SEL3" value="'.$arParams["PARAM_PARSE_SEL3"].'">
		</td>
		</tr>';
		$html .= '<tr>
		<td>
			'.Loc::getMessage("MLIFE_PARSER_OPTION_UN_5").'
		</td>
		<td>
			<input type="text" name="PARAM_PARSE_SEL4" value="'.$arParams["PARAM_PARSE_SEL4"].'">
		</td>
		</tr>';
		$html .= '<tr>
		<td>
			'.Loc::getMessage("MLIFE_PARSER_OPTION_UN_6").'
		</td>
		<td>
			<input type="text" name="PARAM_PARSE_SEL5" value="'.$arParams["PARAM_PARSE_SEL5"].'">
		</td>
		</tr>';
		
		return $html;
		
	}
	
}
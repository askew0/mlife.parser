<?
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser\Options\yandex;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Parser extends \Mlife\Parser\Parser {
	
	public function startParser($profileId){
		
		self::$profileId = $profileId;
		
		if(intval($_REQUEST["elid"]) > 0) {
			self::$arFilterId = intval($_REQUEST["elid"]);
		}
		
		if(!$_REQUEST["option"] || $_REQUEST["option"]=="get_count"){
		
			$count = self::getCount($profileId);
			
			if($count>0) {
				
				$this->getMess(false,$profileId,array("mess"=>sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR4"),$count),"option"=>"get_url"),true);
			
			}else{
				
				$this->getMess("001",$profileId,array(),false);
			
			}
			
		}elseif($_REQUEST["option"]=="get_url"){
			
			$urlList = self::getUrl($profileId);

			if(!$urlList) {
				$this->getMess("002",$profileId,array(),false);
			}else{
				
				foreach($urlList as $link){
					
					$contentOb = new \Mlife\Parser\ContentCurl();
					
					$proxy = false;
					
					if(\COption::GetOptionString("mlife.parser", "proxyotp", "N")=="Y"){
					
						$proxer = \Mlife\Parser\ProxyTable::getList(
							array(
								'select' => array("PROXY"),
								'order' => array("ID"=>"DESC"),
								'limit' => 1
							)
						)->Fetch();
						
						if(isset($proxer["PROXY"])) $proxy = $proxer["PROXY"];
					
					}
					
					if(self::$arFilterId && \COption::GetOptionString("mlife.parser", "proxyotpel", "N")=="Y") {
						$proxy = false;
					}else{
						if(!$proxy && \COption::GetOptionString("mlife.parser", "proxyotp", "N")=="Y") {
							$this->getMess("005",$profileId,array(),false);
							die();
						}
					}
					
					if($proxy){
					
						$contentOb->n_proxy = 1;
						$contentOb->use_proxy_list = true;
						$contentOb->array_proxy = array($proxy);
					
					}
					
					$contentOb->get($link["URL"],null,null,$link);
					$result = $contentOb->execute();
					$func = $link["FUNC_PARSE"]; //������ ������� ��� ��������� ����������
					call_user_func_array(array($this,$func),$result);
					
					break;
				}
				
			}
			
		}elseif($_REQUEST["option"]=="get_capcha"){
			
			$urlList = self::getUrl($profileId);
			
			if(!$urlList) {
				$this->getMess("002",$profileId,array(),false);
			}else{
				
				foreach($urlList as $link){
					
					$contentOb = new \Mlife\Parser\ContentCurl();
					
					$proxy = false;
					
					if(\COption::GetOptionString("mlife.yandexload", "proxyotp", "N")=="Y"){
					
						$proxer = \Mlife\Parser\ProxyTable::getList(
							array(
								'select' => array("PROXY"),
								'order' => array("ID"=>"DESC"),
								'limit' => 1
							)
						)->Fetch();
						
						if(isset($proxer["PROXY"])) $proxy = $proxer["PROXY"];
					
					}
					
					if(self::$arFilterId && \COption::GetOptionString("mlife.parser", "proxyotpel", "N")=="Y") {
						$proxy = false;
					}else{
						if(!$proxy && \COption::GetOptionString("mlife.yandexload", "proxyotp", "N")=="Y") {
							$this->getMess("005",$profileId,array(),false);
							die();
						}
					}
					
					if($proxy){
					
						$contentOb->n_proxy = 1;
						$contentOb->use_proxy_list = true;
						$contentOb->array_proxy = array($proxy);
					
					}
					
					$link["URL"] = "http://market.".\COption::GetOptionString("mlife.parser","yandex_show_capcha_url","yandex.ru")."/checkcaptcha?retpath=".urlencode($_REQUEST["retpath"])."&key=".urlencode($_REQUEST["key"])."&rep=".urlencode($_REQUEST["rep"]);
					$link["FUNC_PARSE"] = $_REQUEST["func"];
					
					$contentOb->get($link["URL"],null,null,$link);
					$result = $contentOb->execute();
					$func = $link["FUNC_PARSE"]; //������ ������� ��� ��������� ����������
					call_user_func_array(array($this,$func),$result);
					
					break;
				}
			}
			
		}
		
	}
	
	public function getUrl($profileId){
		
		$arParams = self::getProfileParams($profileId);
		if(!$arParams) {
			$this->getMess("003",$profileId,array(),false);
		}else{
			
			\CModule::IncludeModule("iblock");
			$arFilter = array();
			$arFilter["IBLOCK_ID"] = $arParams["IBLOCK"];
			if(intval($arParams["CATEGORY"])>0) {
				$arFilter["SECTION_ID"] = $arParams["CATEGORY"];
				$arFilter["INCLUDE_SUBSECTIONS"] = "Y";
			}
			$image = false;
			$arFilter[] = array("LOGIC"=>"AND", array("!PROPERTY_YANDEX_ID" => false), array("!PROPERTY_YANDEX_ID" => 'no'));
			if($arParams["PARAM_EL_DETAIL_PICTURE"][1] || $arParams["PARAM_EL_PREVIEW_PICTURE"][1]){
				$arFilter[] = array("LOGIC"=>"OR",array("!PROPERTY_YA_ACTIVE_HAR_VALUE" => "Y"), array("!PROPERTY_YA_ACTIVE_IMG_VALUE" => "Y"));
				$image = true;
			}else{
				$arFilter[] = array("!PROPERTY_YA_ACTIVE_HAR_VALUE" => "Y");
			}
			if(self::$arFilterId) $arFilter['ID'] = self::$arFilterId;
			$arLinks = array();
			$res = \CIBlockElement::GetList(array("rand"=>"asc"),$arFilter,false,Array("nTopCount"=>self::$limitLink),array("ID","PROPERTY_YANDEX_ID","PROPERTY_YA_ACTIVE_HAR",
			"PROPERTY_YA_ACTIVE_IMG"));
			while($ar = $res->GetNext(false,false)){
				if(strpos($ar["PROPERTY_YANDEX_ID_VALUE"],"yandex") !== false){
					$ar["PROPERTY_YANDEX_ID_VALUE"] = preg_replace("/.*modelid=([0-9]+)&.*/is","$1",$ar["PROPERTY_YANDEX_ID_VALUE"]);
				}
				if(!$ar["PROPERTY_YA_ACTIVE_HAR_VALUE"]){
					$arLinks[] = array("URL"=>"http://market.yandex.ru/model-spec.xml?modelid=".$ar["PROPERTY_YANDEX_ID_VALUE"],"ELEMENT_ID"=>$ar["ID"],
					"FUNC_PARSE"=>"contentHar");
				}
				if($image){
					if(!$ar["PROPERTY_YA_ACTIVE_IMG_VALUE"]){
						$arLinks[] = array("URL"=>"http://market.yandex.ru/model.xml?modelid=".$ar["PROPERTY_YANDEX_ID_VALUE"],"ELEMENT_ID"=>$ar["ID"],
						"FUNC_PARSE"=>"contentImage");
					}
				}
			}
			
			if(count($arLinks)==0) return false;
			return $arLinks;
			
		}
		
	}
	
	//��������������
	public function contentHar($response, $info, $request){
		
		$arParams = self::$arParams;
		$profileId = self::$profileId;
		
		if($info['http_code']!==200)
		{
			//������� ������
			if($request["proxy"]){
				\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
			}
			
			if($info['http_code'] == 404){
				
				\CModule::IncludeModule('iblock');
				
				$arLoadprops = array();
				$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_HAR", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
				if($ar_enum_list = $db_enum_list->GetNext()){
					$arLoadprops['YA_ACTIVE_HAR'] = $ar_enum_list["ID"];
					\CIBlockElement::SetPropertyValuesEx($request[5]['ELEMENT_ID'], $arParams["IBLOCK"], $arLoadprops);
				}
				
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR5"),$request["proxy"],$request[5]['ELEMENT_ID'],$info['http_code']);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
				
			}else{
			
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR6"),$request["proxy"],$info['http_code']);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_url"),true,$request['url']);
			
			}
			
		}else{
			
			$saw = new \Mlife\Parser\nokogiri($response);
			
			if($prop = $saw->get('.b-captcha')->toArray()) {
				
				if(\COption::GetOptionString("mlife.parser","yandex_show_capcha","Y")=="Y" && self::$arFilterId){
				
				$defUrl = preg_replace("/(.*)market\.(.*)\/(.*)/","$2",$request[0]);
				\COption::SetOptionString("mlife.parser","yandex_show_capcha_url",$defUrl);
				
				$html = $saw->get('.b-captcha')->toXml();
				global $APPLICATION;
				$html = str_replace('action="/checkcaptcha"','action="'.$APPLICATION->GetCurPageParam("option=get_capcha&func=contentHar",array("option")).'"',$html);
				$html = str_replace("GET","POST",$html);
				
				if(ToLower(SITE_CHARSET) != 'utf-8'){
					$html = $GLOBALS["APPLICATION"]->ConvertCharset($html, "UTF-8", SITE_CHARSET);
				}
				
				$img = $saw->get(".b-captcha__layout__l")->toArray();
				$img = $img[0]["img"][0]["src"];
				
					$contentOb = new \Mlife\Parser\ContentCurl();
					$proxy = false;
					if(\COption::GetOptionString("mlife.yandexload", "proxyotp", "N")=="Y"){
						$proxer = \Mlife\Parser\ProxyTable::getList(
							array(
								'select' => array("PROXY"),
								'order' => array("ID"=>"DESC"),
								'limit' => 1
							)
						)->Fetch();
						if(isset($proxer["PROXY"])) $proxy = $proxer["PROXY"];
					}
					if(self::$arFilterId && \COption::GetOptionString("mlife.parser", "proxyotpel", "N")=="Y") {
						$proxy = false;
					}else{
						if(!$proxy && \COption::GetOptionString("mlife.yandexload", "proxyotp", "N")=="Y") {
							$this->getMess("005",$profileId,array(),false);
							die();
						}
					}
					if($proxy){
						$contentOb->n_proxy = 1;
						$contentOb->use_proxy_list = true;
						$contentOb->array_proxy = array($proxy);
					}
					$contentOb->get($img,null,null,array());
					$result = $contentOb->execute();
					$tmp_name = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp274s_yandex_capcha.jpg";
					file_put_contents($tmp_name,$result[0]);
					
					$html = preg_replace("/(.*src=\")(.*)(\".*b-captcha__image.*)/is","$1".str_replace($_SERVER["DOCUMENT_ROOT"],"",$tmp_name)."$3",$html);
					$html .= '<style>.b-link_captcha_reload {display:none;}</style>';
					
					print_r($html);die();
				
				}else{
					
					//������� ������
					if($request["proxy"]){
						\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
					}
					
				}
				
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR7"),$request["proxy"],$info['http_code']);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_url"),true,$request['url']);
				
				//echo'<pre>';print_r($saw->get('.b-captcha'));echo'</pre>';
			}
			
			elseif($prop = $saw->get('table.b-properties')->toArray()){
				
				if(is_array($prop[0]['tbody']['tr']) && count($prop[0]['tbody']['tr']>0)){
					$g = 0;
					foreach($prop[0]['tbody']['tr'] as $val){
						if(isset($val['th']['class']) && ($val['th']['class']=='b-properties__title')){
							$g++;
							$groupValue[$g] = $val['th']['#text'];
							if(ToLower(SITE_CHARSET) != 'utf-8') $groupValue[$g] = $GLOBALS["APPLICATION"]->ConvertCharset($groupValue[$g], "UTF-8", SITE_CHARSET);
							$i = 1;
							if($g==1){
								$manufAr = $saw->get("#guru")->toArray();
								$manuf = $manufAr[0]['a']['span']['#text'];
								$propValue[$g][$i]['name'] = Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_MANUF");
								if(ToLower(SITE_CHARSET) != 'utf-8'){
									$propValue[$g][$i]['val'] = $GLOBALS["APPLICATION"]->ConvertCharset($manuf, "UTF-8", SITE_CHARSET);
								}else{
									$propValue[$g][$i]['val'] = $manuf;
								}
								$i++;
							}
							
						}elseif(is_array($val['th'][0]) && $val['th'][0]['class']=='b-properties__label b-properties__label-title'){
							$propValue[$g][$i]['name'] = $val['th'][0]['span']['#text'];
							if(ToLower(SITE_CHARSET) != 'utf-8') $propValue[$g][$i]['name'] = $GLOBALS["APPLICATION"]->ConvertCharset($propValue[$g][$i]['name'], "UTF-8", SITE_CHARSET);
							$propValue[$g][$i]['name'] = preg_replace('/ {2,}/',' ',$propValue[$g][$i]['name']);
							$propValue[$g][$i]['val'] = $val['td'][0]['#text'];
							if(ToLower(SITE_CHARSET) != 'utf-8') $propValue[$g][$i]['val'] = $GLOBALS["APPLICATION"]->ConvertCharset($propValue[$g][$i]['val'], "UTF-8", SITE_CHARSET);
							$i++;
						}
					}
				}
				$resa = $this->loadProductHar($propValue,$groupValue,$request[5]['ELEMENT_ID']);
				if($resa) {

					$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR8"),$request[5]['ELEMENT_ID']);
					$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
					
				}else{
					
					\CModule::IncludeModule('iblock');
				
					$arLoadprops = array();
					$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_HAR", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
					if($ar_enum_list = $db_enum_list->GetNext()){
						$arLoadprops['YA_ACTIVE_HAR'] = $ar_enum_list["ID"];
						\CIBlockElement::SetPropertyValuesEx($request[5]['ELEMENT_ID'], $arParams["IBLOCK"], $arLoadprops);
					}
					
					$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR9"),$request[5]['ELEMENT_ID']);
					$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
					
				}
				
			}
			
			else{
			
				if($request["proxy"]){
					\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
				}
				
				$arLoadprops = array();
				$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_HAR", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
				if($ar_enum_list = $db_enum_list->GetNext()){
					$arLoadprops['YA_ACTIVE_HAR'] = $ar_enum_list["ID"];
					\CIBlockElement::SetPropertyValuesEx($request[5]['ELEMENT_ID'], $arParams["IBLOCK"], $arLoadprops);
				}
				
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR10"),$request[5]['ELEMENT_ID'],$request["proxy"]);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
				
			}
			
		}
		
	}
	
	//�����������
	public function contentImage($response, $info, $request){
		
		$arParams = self::$arParams;
		$profileId = self::$profileId;
		
		if($info['http_code']!==200)
		{
			//������� ������
			if($request["proxy"]){
				\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
			}
			
			if($info['http_code'] == 404){
				
				\CModule::IncludeModule('iblock');
				
				$arLoadprops = array();
				$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_IMG", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
				if($ar_enum_list = $db_enum_list->GetNext()){
					$arLoadprops['YA_ACTIVE_IMG'] = $ar_enum_list["ID"];
					\CIBlockElement::SetPropertyValuesEx($request[5]['ELEMENT_ID'], $arParams["IBLOCK"], $arLoadprops);
				}
				
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR5"),$request["proxy"],$request[5]['ELEMENT_ID'],$info['http_code']);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
				
			}else{
			
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR6"),$request["proxy"],$info['http_code']);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_url"),true,$request['url']);
			
			}
			
		}else{
		
			$saw = new \Mlife\Parser\nokogiri($response);
			
			if($prop = $saw->get('.b-captcha')->toArray()) {
				
				
				if(\COption::GetOptionString("mlife.parser","yandex_show_capcha","Y")=="Y" && self::$arFilterId){
				$defUrl = preg_replace("/(.*)market\.(.*)\/(.*)/","$2",$request[0]);
				\COption::SetOptionString("mlife.parser","yandex_show_capcha_url",$defUrl);
				$html = $saw->get('.b-captcha')->toXml();
				global $APPLICATION;
				$html = str_replace('action="/checkcaptcha"','action="'.$APPLICATION->GetCurPageParam("option=get_capcha&func=contentImage",array("option")).'"',$html);
				$html = str_replace("GET","POST",$html);
				
				if(ToLower(SITE_CHARSET) != 'utf-8'){
					$html = $GLOBALS["APPLICATION"]->ConvertCharset($html, "UTF-8", SITE_CHARSET);
				}
				
				$img = $saw->get(".b-captcha__layout__l")->toArray();
				$img = $img[0]["img"][0]["src"];
				
					$contentOb = new \Mlife\Parser\ContentCurl();
					$proxy = false;
					if(\COption::GetOptionString("mlife.yandexload", "proxyotp", "N")=="Y"){
						$proxer = \Mlife\Parser\ProxyTable::getList(
							array(
								'select' => array("PROXY"),
								'order' => array("ID"=>"DESC"),
								'limit' => 1
							)
						)->Fetch();
						if(isset($proxer["PROXY"])) $proxy = $proxer["PROXY"];
					}
					if(self::$arFilterId && \COption::GetOptionString("mlife.parser", "proxyotpel", "N")=="Y") {
						$proxy = false;
					}else{
						if(!$proxy && \COption::GetOptionString("mlife.yandexload", "proxyotp", "N")=="Y") {
							$this->getMess("005",$profileId,array(),false);
							die();
						}
					}
					if($proxy){
						$contentOb->n_proxy = 1;
						$contentOb->use_proxy_list = true;
						$contentOb->array_proxy = array($proxy);
					}
					$contentOb->get($img,null,null,array());
					$result = $contentOb->execute();
					$tmp_name = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp274s_yandex_capcha.jpg";
					file_put_contents($tmp_name,$result[0]);
					
					$html = preg_replace("/(.*src=\")(.*)(\".*b-captcha__image.*)/is","$1".str_replace($_SERVER["DOCUMENT_ROOT"],"",$tmp_name)."$3",$html);
					$html .= '<style>.b-link_captcha_reload {display:none;}</style>';
					
					print_r($html);die();
				
				}else{
					
					//������� ������
					if($request["proxy"]){
						\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
					}
					
				}
				
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR7"),$request["proxy"],$info['http_code']);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_url"),true,$request['url']);
				
				
			}
			elseif($image = $saw->get("span.b-model-pictures__big")->toArray()){
			
				$imgsrc = false;
				if(isset($image[0]['a']['href'])) {
					$imgsrc = $image[0]['a']['href'];
				}elseif(isset($image[0]['img'][0]['src'])){
					$imgsrc = $image[0]['img'][0]['src'];
				}
				if($imgsrc){
					//print_r($imgsrc);die();
					$resa = $this->loadProductImage($imgsrc,$request[5]['ELEMENT_ID']);
					if($resa) {
					
						$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR11"),$request[5]['ELEMENT_ID']);
						$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
						
					}else{
						
						$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR12"),$request[5]['ELEMENT_ID']);
						$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
						
					}
				}
			
			}else{
				
				if($request["proxy"]){
					\Mlife\Parser\ProxyTable::deleteByProxy($request["proxy"]);
				}
				
				$arLoadprops = array();
				$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_IMG", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
				if($ar_enum_list = $db_enum_list->GetNext()){
					$arLoadprops['YA_ACTIVE_IMG'] = $ar_enum_list["ID"];
					\CIBlockElement::SetPropertyValuesEx($request[5]['ELEMENT_ID'], $arParams["IBLOCK"], $arLoadprops);
				}
				
				$mess = sprintf(Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR10"),$request[5]['ELEMENT_ID'],$request["proxy"]);
				$this->getMess(false,$profileId,array("mess"=>$mess,"option"=>"get_count"),true,$request['url']);
				
			}
			
		}
		
	}
	
	public function loadProductImage($image_src,$elId){
		
		\CModule::IncludeModule("iblock");
		$obOption = new \Mlife\Parser\Options\yandex\Option();
		$CLASS_FUNCTIONS = $obOption::$CLASS_FUNCTIONS;
		
		$arParams = self::$arParams;
		
		if($arParams["PARAM_EL_DETAIL_PICTURE"][1] || $arParams["PARAM_EL_PREVIEW_PICTURE"][1]){
			
			if(strpos($image_src,".png") !== false) {
				$tmp_name = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp274s_yandex.png";
			}elseif(strpos($image_src,".gif") !== false) {
				$tmp_name = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp274s_yandex.gif";
			}else{
				$tmp_name = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp274s_yandex.jpg";
			}
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_POST, 0);
			curl_setopt($ch,CURLOPT_URL,$image_src);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$im=curl_exec($ch);
			curl_close($ch);
			file_put_contents($tmp_name,$im);
			$image_src = $tmp_name;
			
			if($arParams["PARAM_EL_DETAIL_PICTURE"][1]){
				if(method_exists($CLASS_FUNCTIONS,$arParams["PARAM_EL_DETAIL_PICTURE"][1])){
					$aaa = $CLASS_FUNCTIONS::$arParams["PARAM_EL_DETAIL_PICTURE"][1]("PARAM_EL_DETAIL_PICTURE",$elId,$arParams["IBLOCK"],$image_src,$arParams["PARAM_EL_DETAIL_PICTURE"][2]);
					if($aaa){
						$arLoadField["DETAIL_PICTURE"] = $aaa;
					}
				}
			}
			if($arParams["PARAM_EL_PREVIEW_PICTURE"][1]){
				if(method_exists($CLASS_FUNCTIONS,$arParams["PARAM_EL_PREVIEW_PICTURE"][1])){
					$aaa = $CLASS_FUNCTIONS::$arParams["PARAM_EL_PREVIEW_PICTURE"][1]("PARAM_EL_PREVIEW_PICTURE",$elId,$arParams["IBLOCK"],$image_src,$arParams["PARAM_EL_PREVIEW_PICTURE"][2]);
					if($aaa){
						$arLoadField["PREVIEW_PICTURE"] = $aaa;
					}
				}
			}
			
			if(count($arLoadField)>0){
				$el = new \CIBlockElement;
				$res = $el->Update($elId, $arLoadField);
				
				$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_IMG", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
				if($ar_enum_list = $db_enum_list->GetNext()){
					$arLoadprops['YA_ACTIVE_IMG'] = $ar_enum_list["ID"];
					\CIBlockElement::SetPropertyValuesEx($elId, $arParams["IBLOCK"], $arLoadprops);
				}
				if(!$res) {
					return false;
				}
			}
			
			unlink($image_src);
		
		}
		
		return true;
		
	}
	
	public function loadProductHar($propValue,$groupValue,$elementId){
		
		\CModule::IncludeModule("iblock");
		
		$obOption = new \Mlife\Parser\Options\yandex\Option();
		$CLASS_FUNCTIONS = $obOption::$CLASS_FUNCTIONS;
		
		$arParams = self::$arParams;
		$arParamsTitle = array();
		foreach($arParams as $key=>$val){
			$arParamsTitle[$key] = $val[0];
		}
		
		$arLoadprops = array();
		foreach($propValue as $groupId=>$propGroup){
			foreach($propGroup as $prop){
				if($prop['name']){
					$key = array_search(trim($prop['name']),$arParamsTitle);
					$keyAr = array_keys($arParamsTitle,trim($prop['name']));
					if(count($keyAr)==0 && $key) $keyAr[] = $key;
					
					if($key){
						foreach($keyAr as $key){
							
							$opt = $arParams[$key];
							if($opt[1]){
								if(substr($key,0,9)=="PARAM_EL_"){
									$paramId = substr($key,9,60);
								}elseif(substr($key,0,6)=="PARAM_"){
									$propId = substr($key,6,60);
									//$opt[1] - �������
									//$opt[2] - ��������� ��� �������
									//����� ������� ��������� ������ � ���������� ������� �������
									if(method_exists($CLASS_FUNCTIONS,$opt[1])){
										$aaa = $CLASS_FUNCTIONS::$opt[1]($propId,$elementId,$arParams["IBLOCK"],$prop['val'],$opt[2]);
										if($aaa) $arLoadprops[$propId] = $aaa;
									}
								}
							}
							
						}
					}
				}
			}
		}
		
		$key = array_search("TABLE",$arParamsTitle);
		if($key){
			$html = '<table class="mlifeDescr">';
			if(count($propValue)>0){
				foreach($propValue as $keygroup=>$props) {
					$html .= '<tr><th class="group" colspan="2">'.$groupValue[$keygroup].'</th></tr>';
					foreach($props as $prop) {
						$html .= '<tr><td class="prop_title">'.$prop['name'].'</td><td class="prop_value">'.$prop['val'].'</td></tr>';
					}
				}
			}
			$html .= "</table>";
			$opt = $arParams[$key];
			if(substr($key,0,9)=="PARAM_EL_"){
			
			}elseif(substr($key,0,6)=="PARAM_"){
				
				$propId = substr($key,6,60);
				if(method_exists($CLASS_FUNCTIONS,$opt[1])){
					$aaa = $CLASS_FUNCTIONS::$opt[1]($propId,$elementId,$arParams["IBLOCK"],$html,$opt[2]);
					if($aaa) $arLoadprops[$propId] = $aaa;
				}
				
			}
			
		}
		
		\CIBlockElement::SetPropertyValuesEx($elementId, $arParams["IBLOCK"], $arLoadprops);
		
		$arLoadprops = array();
		$db_enum_list = \CIBlockProperty::GetPropertyEnum("YA_ACTIVE_HAR", Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK"], "VALUE"=>"Y"));
		if($ar_enum_list = $db_enum_list->GetNext()){
			$arLoadprops['YA_ACTIVE_HAR'] = $ar_enum_list["ID"];
			\CIBlockElement::SetPropertyValuesEx($elementId, $arParams["IBLOCK"], $arLoadprops);
		}
		
		return true;
	}
	
	public function getCount($profileId){
		
		$arParams = self::getProfileParams($profileId);
		if(!$arParams) {
			$this->getMess("003",$profileId,array(),false);
		}else{
			\CModule::IncludeModule("iblock");
			$arFilter = array();
			$arFilter["IBLOCK_ID"] = $arParams["IBLOCK"];
			if(intval($arParams["CATEGORY"])>0) {
				$arFilter["SECTION_ID"] = $arParams["CATEGORY"];
				$arFilter["INCLUDE_SUBSECTIONS"] = "Y";
			}
			$arFilter[] = array("LOGIC"=>"AND", array("!PROPERTY_YANDEX_ID" => false), array("!PROPERTY_YANDEX_ID" => 'no'));
			if($arParams["PARAM_EL_DETAIL_PICTURE"][1] || $arParams["PARAM_EL_PREVIEW_PICTURE"][1]){
				$arFilter[] = array("LOGIC"=>"OR",array("!PROPERTY_YA_ACTIVE_HAR_VALUE" => "Y"), array("!PROPERTY_YA_ACTIVE_IMG_VALUE" => "Y"));
			}else{
				$arFilter[] = array("!PROPERTY_YA_ACTIVE_HAR_VALUE" => "Y");
			}
			if(self::$arFilterId) $arFilter['ID'] = self::$arFilterId;
			$res = \CIBlockElement::GetList(array(),$arFilter,array(),false,array());
			return $res;
		}
		
		return 0;
		
	}
	
	public function getMess($messCode,$profileId,$option=array(),$nextUrl=true,$url=false){
		
		$agent = false;
		
		$mess = false;
		
		if($messCode=="001"){
			$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR1");
		}elseif($messCode=="002"){
			$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR2");
		}elseif($messCode=="003"){
			$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR3");
		}elseif($messCode=="005"){
			$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_ERROR005");
		}
		if(isset($option["mess"])){
			$mess = $option["mess"];
		}
		
		if($url) $mess .= " URL=".$url;
		if($mess) \Mlife\Parser\LogerTable::add(array("PARSER"=>"yandex","PROFILE"=>$profileId,"TEXT"=>$mess));
		
		if(!$agent){
			echo $mess."<br/>";
		}
		
		if(!$agent && $nextUrl){
			$nextUrl = '/bitrix/admin/mlife_parser_start.php?lang='.LANG.'&parser='.self::$parserId."&ID=".$profileId."&option=".$option["option"];
			if(self::$arFilterId) $nextUrl .= "&elid=".self::$arFilterId;
			if(trim($_REQUEST["returnUrl"])) $nextUrl .= "&returnUrl=".trim(urlencode($_REQUEST["returnUrl"]));
			echo '<script>
				setTimeout( \'location="'.$nextUrl.'";\', 2000 );
			</script>';
			echo '<br/><a href="'.$nextUrl.'">'.Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_REDIRECTTEXT").'</a>';
			if(trim($_REQUEST["returnUrl"])){
			$startUrl = trim($_REQUEST["returnUrl"]);
			}else{
			$startUrl = '/bitrix/admin/mlife_parser_profile.php?lang='.LANG.'&parser='.self::$parserId."&ID=".$profileId;
			}
			echo '<br/><br/><br/><a href="'.$startUrl.'">'.Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_END").'</a>';
		}elseif(!$agent){
			if(trim(urldecode($_REQUEST["returnUrl"]))) {
				$nextUrl = trim($_REQUEST["returnUrl"]);
				echo '<script>
					setTimeout( \'location="'.$nextUrl.'";\', 4000 );
				</script>';
				echo '<br/><a href="'.$nextUrl.'">'.Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_PARSER_REDIRECTTEXT").'</a>';
			}
		}
		
	}
	
}
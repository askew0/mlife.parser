<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Parser\Options\yandex;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Option extends \Mlife\Parser\Option
{
	const CLASS_PROFILE = '\Mlife\Parser\Profile\YandexTable'; //����� � ��������� �������� �� ������� ��������
	const CLASS_PARSER = '\Mlife\Parser\Options\yandex\Parser'; //����� �������
	
	function __construct() {
		parent::$CLASS_FUNCTIONS = 'Mlife\Parser\Options\yandex\Functions'; //���������������� ������� ��� ��������� ������
	}
	
	public static function getMenu() {
		
		$menu = array(
			array(
				"text" => Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_MENU1"),
				"url" => "mlife_parser_profile.php?lang=".LANGUAGE_ID."&parser=yandex",
				"more_url" => Array("mlife_parser_profile_edit.php?lang=".LANGUAGE_ID."&parser=yandex"),
				"title" => Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_MENU1"),
				"sort" => 100,
			),
		);
		
		return $menu;
		
	}
	
	public function getCtMenu($id,$lang,$parser) {
		
		$arActions = array();
		$arActions[] = array(
			"ICON"=>"edit",
			"DEFAULT"=>true,
			"TEXT"=>Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_MENU3"),
			"TITLE"=>Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_MENU3"),
			"ACTION"=>'mlife_parser_profile_edit.php?ID='.$id.'&lang='.$lang."&parser=".$parser
			);
		$arActions[] = array(
			"ICON"=>"edit",
			"DEFAULT"=>true,
			"TEXT"=> Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_MENU4"),
			"TITLE"=> Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_MENU4"),
			"ACTION"=>'mlife_parser_start.php?ID='.$id.'&lang='.$lang."&parser=".$parser
			);
		$arActions[] = array(
			"ICON"=>"edit",
			"DEFAULT"=>true,
			"TEXT"=> Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_MENU5"),
			"TITLE"=> Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_MENU5"),
			"ACTION"=>'mlife_parser_start.php?ID='.$id.'&lang='.$lang."&parser=".$parser."&DELETELOG=1"
			);
		
		return $arActions;
		
	}
	
	public function getNote(){
		
		$mess = Loc::getMessage("MLIFE_PARSER_OPTIONS_YANDEX_MESS");
		
		return $mess;
		
	}

}
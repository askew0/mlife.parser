<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */
 
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
 
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("mlife.parser");

if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$error = array();

CModule::IncludeModule("mlife.parser");

$profileId = intval($_REQUEST["ID"]);

if($parser = trim($_REQUEST["parser"])){
	$classOptions = "\\Mlife\\Parser\\Options\\".strtolower($parser)."\\Option";
	if(!class_exists($classOptions)) {
		$error[] = $classOptions." not exists";
		$classError = true;
	}else{
		$className = $classOptions::CLASS_PARSER;
		$className = new $className;
		$className::$classProfile = $classOptions::CLASS_PROFILE;
		$className::$parserId = $parser;
	}
}else{
	$error[] = "class not exists";
	$classError = true;
}

if(!$classError){
	
	if(intval($_REQUEST["DELETELOG"])==1 && $profileId>0) \Mlife\Parser\LogerTable::clearLog($parser, $profileId);
	
	$result = $className->startParser($profileId);

	echo $result;

}

if(count($error)>0){
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
	CAdminMessage::ShowMessage(implode(', ',$error));
}
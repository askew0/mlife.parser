<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

global $APPLICATION, $USER, $USER_FIELD_MANAGER;

CModule::IncludeModule("mlife.parser");
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("mlife.parser");

if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$listTableId = "tbl_mlife_parser_loger";

$oSort = new CAdminSorting($listTableId, "ID", "DESC");
$arOrder = (strtoupper($by) === "ID"? array($by => $order): array($by => $order, "ID" => "DESC"));

$adminList = new CAdminList($listTableId, $oSort);

// опишем элементы фильтра
$FilterArr = Array(
    "find_parser",
    "find_profile",
    );

// инициализируем фильтр
$adminList->InitFilter($FilterArr);

$arFilter = array();
if($find_parser){
$arFilter["PARSER"] = $find_parser;
}
if($find_profile){
$arFilter["PROFILE"] = $find_profile;
}

$ASZCurency = \Mlife\Parser\LogerTable::getList(
	array(
		'order' => array("ID"=>"DESC"),
		'filter' => $arFilter,
	)
);

$ASZCurency = new CAdminResult($ASZCurency, $listTableId);
$ASZCurency->NavStart();

$adminList->NavText($ASZCurency->GetNavPrint(Loc::getMessage("MLIFE_PARSER_LOGER_NAV")));

$cols = \Mlife\Parser\LogerTable::getMap();
$colHeaders = array();

foreach ($cols as $colId => $col)
{
	$tmpAr = array(
		"id" => $colId,
		"content" => $col["title"],
		"sort" => false,
		"default" => true,
	);
	$colHeaders[] = $tmpAr;
}
$adminList->AddHeaders($colHeaders);

$visibleHeaderColumns = $adminList->GetVisibleHeaderColumns();
$arUsersCache = array();

while ($arRes = $ASZCurency->GetNext())
{
	$row =& $adminList->AddRow($arRes["ID"], $arRes);
}

$adminList->AddFooter(
	array(
		array(
			"title" => Loc::getMessage("MAIN_ADMIN_LIST_SELECTED"),
			"value" => $ASZCurency->SelectedRowsCount()
		),
		array(
			"counter" => true,
			"title" => Loc::getMessage("MAIN_ADMIN_LIST_CHECKED"),
			"value" => "0"
		),
	)
);

$adminList->CheckListMode();

$APPLICATION->SetTitle(Loc::getMessage("MLIFE_PARSER_LOGER_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

?>
<?
$oFilter = new CAdminFilter(
  $listTableId."_filter",
  array(
    "PARSER",
    "PROFILE",
  )
);
?>

<form name="find_form" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
<?
$oFilter->Begin();
?>
<tr>
  <td><?=Loc::getMessage("MLIFE_PARSER_LOGER_FILTER1")?>:</td>
  <td>
    <input type="text" name="find_parser" size="47" value="<?echo htmlspecialchars($find_parser)?>">
  </td>
</tr>
<tr>
  <td><?=Loc::getMessage("MLIFE_PARSER_LOGER_FILTER2")?></td>
  <td><input type="text" name="find_profile" size="47" value="<?echo htmlspecialchars($find_profile)?>"></td>
</tr>
<?
$oFilter->Buttons(array("table_id"=>$sTableID,"url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
$oFilter->End();
?>
</form>
<?
$adminList->DisplayList();
?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>
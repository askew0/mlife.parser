<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

IncludeModuleLangFile(__FILE__);

CModule::IncludeModule("mlife.parser");

$POST_RIGHT = $APPLICATION->GetGroupRight("mlife.parser");
/*
$FilterSiteId = false;
if($ASZmodule = CModule::IncludeModule("mlife.asz")){
	if($POST_RIGHT == "D") {
	$arSites = \Mlife\Asz\Functions::GetGroupRightSiteId();
		if(count($arSites)>0) $FilterSiteId = $arSites;
		if($FilterSiteId) $POST_RIGHT = "W";
	}
}
*/

$parentMenuId = "global_menu_services";
if($ASZmodule){
//$parentMenuId = "global_menu_mlifeasz";
}
$aMenu = array();
$arProfiles = array();
$filelist = glob($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.parser/lib/options/*");
$i=0;
foreach ($filelist as $profileName) {
$i++;
	$profileName = str_replace($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/mlife.parser/lib/options/','',$profileName);
	$className = "\\Mlife\\Parser\\Options\\".$profileName."\\Option";
	if(method_exists($className, "getMenu")){
		$tempAr = array(
			"parent_menu" => $parentMenuId,
			"section" => "mlife_parser_".strtolower($profileName),
			"sort" => (800+$i),
			"module_id" => "mlife.parser",
			"text" => GetMessage("MLIFE_PARSER_MENU_NAME1")." - ".strtolower($profileName),
			"title" => GetMessage("MLIFE_PARSER_MENU_NAME1")." - ".strtolower($profileName),
			"items_id" => "mlife_parser_".strtolower($profileName),
		);
		$tempAr["items"] = $className::getMenu();
		$aMenu[] = $tempAr;
	}
}
$aMenu[] = array(
			"parent_menu" => $parentMenuId,
			"section" => "mlife_parser_main",
			"sort" => 100,
			"module_id" => "mlife.parser",
			"text" => GetMessage("MLIFE_PARSER_MENU_NAME2"),
			"title" => GetMessage("MLIFE_PARSER_MENU_NAME2"),
			"items_id" => "mlife_parser_main",
			"items" => array(
				array(
					"text" => GetMessage("MLIFE_PARSER_MENU_NAME3"),
					"url" => "mlife_parser_proxy.php?lang=".LANGUAGE_ID,
					"more_url" => Array("mlife_parser_proxy_edit.php?lang=".LANGUAGE_ID),
					"title" =>  GetMessage("MLIFE_PARSER_MENU_NAME3"),
					"sort" => 100,
				),
				array(
					"text" => GetMessage("MLIFE_PARSER_MENU_NAME4"),
					"url" => "mlife_parser_loger.php?lang=".LANGUAGE_ID,
					"more_url" => Array(),
					"title" =>  GetMessage("MLIFE_PARSER_MENU_NAME4"),
					"sort" => 100,
				),
				array(
					"text" => GetMessage("MLIFE_PARSER_MENU_NAME5"),
					"url" => "settings.php?lang=".LANGUAGE_ID."&mid=mlife.parser&mid_menu=1",
					"more_url" => Array(),
					"title" =>  GetMessage("MLIFE_PARSER_MENU_NAME5"),
					"sort" => 100,
				),
			)
		);

if($POST_RIGHT != "D") 
	return $aMenu;
?>

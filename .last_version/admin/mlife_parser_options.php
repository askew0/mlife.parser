<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.parser
 * @copyright  2014 Zahalski Andrew
 */

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

CModule::IncludeModule("mlife.parser");
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("mlife.parser");

if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?
$aTabs = array(
  array("DIV" => "edit1", "TAB" => Loc::getMessage("MLIFE_PARSER_OPT_TAB1"), "ICON"=>"main_user_edit", "TITLE"=>Loc::getMessage("MLIFE_PARSER_OPT_TAB1")),
);
$aTabs[] = array("DIV" => "edit4", "TAB" => GetMessage("MLIFE_PARSER_OPT_TAB4"), "ICON" => "vote_settings2", "TITLE" => GetMessage("MLIFE_PARSER_OPT_TAB4"));


if($REQUEST_METHOD == "POST" && $_REQUEST['Update']=="Y" && $POST_RIGHT=="W"){
	
	$proxyotp = (trim($_REQUEST["proxyotp"])=="Y") ? "Y" : "N";
	\COption::SetOptionString("mlife.parser", "proxyotp", $proxyotp);
	
	$proxyotpel = (trim($_REQUEST["proxyotpel"])=="Y") ? "Y" : "N";
	\COption::SetOptionString("mlife.parser", "proxyotpel", $proxyotpel);
	
	$limit1 = intval($_REQUEST["limit1"]);
	\COption::SetOptionString("mlife.parser", "limit1", $limit1);
	
	$limit2 = intval($_REQUEST["limit2"]);
	\COption::SetOptionString("mlife.parser", "limit2", $limit2);
	
}

$APPLICATION->SetTitle(Loc::getMessage("MLIFE_PARSER_OPT_PARAM_TITLE"));

?>

<form method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<?=LANGUAGE_ID?>" id="options">
<?
$tabControl = new CAdminTabControl("tabControl", $aTabs,false,true);
$tabControl->Begin();
?>

<?
$tabControl->BeginNextTab();
?>
	<tr>
		<td width="40%"><?=Loc::getMessage("MLIFE_PARSER_OPT_PARAM_TITLE_1")?>:</td>
		<td><?$val = \COption::GetOptionString("mlife.parser", "proxyotp", "N");?>
			<input type="checkbox" value="Y" name="proxyotp" id="proxyotp" <?if ($val=="Y") echo "checked";?>>
		</td>
	</tr>
	<tr>
		<td width="40%"><?=Loc::getMessage("MLIFE_PARSER_OPT_PARAM_TITLE_2")?>:</td>
		<td><?$val = \COption::GetOptionString("mlife.parser", "proxyotpel", "N");?>
			<input type="checkbox" value="Y" name="proxyotpel" id="proxyotpel" <?if ($val=="Y") echo "checked";?>>
		</td>
	</tr>
	<tr>
		<td width="40%"><?=Loc::getMessage("MLIFE_PARSER_OPT_PARAM_TITLE_3")?>:</td>
		<td><?$val = \COption::GetOptionString("mlife.parser", "limit1", "15");?>
			<input type="text" value="<?=$val?>" name="limit1" id="limit1">
		</td>
	</tr>
	<tr>
		<td width="40%"><?=Loc::getMessage("MLIFE_PARSER_OPT_PARAM_TITLE_4")?>:</td>
		<td><?$val = \COption::GetOptionString("mlife.parser", "limit2", "6");?>
			<input type="text" value="<?=$val?>" name="limit2" id="limit2">
		</td>
	</tr>

<?
$tabControl->BeginNextTab();
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
$tabControl->Buttons();
?>
	<input <?if ($MODULE_RIGHT<"W") echo "disabled" ?> type="submit" class="adm-btn-green" name="Update" value="<?=GetMessage("MLIFE_PARSER_OPT_SEND")?>" />
	<input type="hidden" name="Update" value="Y" />
<?$tabControl->End();
?>
</form>
<?echo BeginNote();?>
<?echo Loc::getMessage("MLIFE_PARSER_OPT_NOTE")?>
<?echo EndNote();?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>
CREATE TABLE IF NOT EXISTS `mlife_parser_loger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PARSER` varchar(20) NOT NULL,
  `PROFILE` int(7) NOT NULL,
  `TEXT` varchar(3000) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `PARSER` (`PARSER`),
  KEY `PROFILE` (`PROFILE`)
);
CREATE TABLE IF NOT EXISTS `mlife_parser_profile_yandex` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK` int(7) NOT NULL,
  `CATEGORY` int(7) DEFAULT NULL,
  `PARAMS` varchar(12000) NOT NULL,
  PRIMARY KEY (`ID`)
);
CREATE TABLE IF NOT EXISTS `mlife_parser_profile_universal` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK` int(7) NOT NULL,
  `CATEGORY` int(7) DEFAULT NULL,
  `PARAMS` varchar(12000) NOT NULL,
  PRIMARY KEY (`ID`)
);
CREATE TABLE IF NOT EXISTS `mlife_parser_proxy` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROXY` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `PROXY` (`PROXY`)
);
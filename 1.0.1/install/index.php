<?
IncludeModuleLangFile(__FILE__);

class mlife_parser extends CModule
{
        var $MODULE_ID = "mlife.parser";
        var $MODULE_VERSION;
        var $MODULE_VERSION_DATE;
        var $MODULE_NAME;
        var $MODULE_DESCRIPTION;

        function mlife_parser() {
				$path = str_replace("\\", "/", __FILE__);
				$path = substr($path, 0, strlen($path) - strlen("/index.php"));
				include($path."/version.php");
				
				$this->MODULE_VERSION = $arModuleVersion["VERSION"];
				$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
				$this->PARTNER_NAME = GetMessage("MLIFE_PARSER_PARTNER_NAME");
				$this->PARTNER_URI = GetMessage("MLIFE_PARSER_PARTNER_URI");
				$this->MODULE_NAME = GetMessage("MLIFE_PARSER_MODULE_NAME");
				$this->MODULE_DESCRIPTION = GetMessage("MLIFE_PARSER_MODULE_DESC");
				
			return true;
        }

        function DoInstall() {
			
			global $USER, $APPLICATION;
			
			if ($USER->IsAdmin())
			{
				if ($this->InstallDB())
				{
					RegisterModule("mlife.parser");
					RegisterModuleDependences("main", "OnAdminContextMenuShow", "mlife.parser", '\Mlife\Parser\Handlers', "OnAdminContextMenuShow");
					$this->InstallFiles();
				}
				$GLOBALS["errors"] = $this->errors;
				$APPLICATION->IncludeAdminFile(GetMessage("MLIFE_PARSER_MODULE_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.parser/install/step.php");
			}
			
        }

        function DoUninstall() {
			global $DB, $USER, $DOCUMENT_ROOT, $APPLICATION, $step;
			if ($USER->IsAdmin())
			{
				$step = IntVal($step);
				if ($step < 2)
				{
					$APPLICATION->IncludeAdminFile(GetMessage("MLIFE_PARSER_MODULE_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.parser/install/unstep1.php");
				}
				elseif ($step == 2)
				{
					$this->UnInstallDB(array(
						"save_tables" => $_REQUEST["save_tables"],
					));
					UnRegisterModule("mlife.parser");
					UnRegisterModuleDependences("main", "OnAdminContextMenuShow", "mlife.parser", '\Mlife\Parser\Handlers', "OnAdminContextMenuShow");
					
					$this->UnInstallFiles();
					$GLOBALS["errors"] = $this->errors;
					$APPLICATION->IncludeAdminFile(GetMessage("MLIFE_PARSER_MODULE_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.parser/install/unstep2.php");
				}
			}
        }
	
	function InstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;

		$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.parser/install/db/".strtolower($DB->type)."/install.sql");

		if ($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("<br>", $this->errors));
			return false;
		}
		return true;
	}
	
	function UnInstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;
		if (!array_key_exists("save_tables", $arParams) || $arParams["save_tables"] != "Y")
		{
			$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.parser/install/db/".strtolower($DB->type)."/uninstall.sql");
		}

		if ($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("<br>", $this->errors));
			return false;
		}
		return true;
	}
	
	function InstallFiles($arParams = array())
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.parser/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
		return true;
	}
	
	function UnInstallFiles()
	{
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.parser/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
		return true;
	}
}

?>

